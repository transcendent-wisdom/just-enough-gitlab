# Managing Terraform Modules: Just Enough Gitlab

This repository serves as an example of how to set up CICD pipeline in Gitlab. To understand the full process and how it works, please refer to the accompanying article on Medium titled "Publishing Terraform Modules to GitLab Infra Registry" found at the following link: [Managing Terraform Modules: Just Enough Gitlab](https://medium.com/p/3145d0ee56e)

